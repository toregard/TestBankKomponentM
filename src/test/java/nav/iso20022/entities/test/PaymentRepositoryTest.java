package nav.iso20022.entities.test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

//import nav.iso20022.entities.*;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.support.AnnotationConfigContextLoader;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.util.Assert;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = { RepositoryConfig.class }, loader = AnnotationConfigContextLoader.class)
//@ActiveProfiles("test")
//@Transactional
public class PaymentRepositoryTest {
//
//    @Inject
//    private Pain001XMLDAO pain001XMLDAO;
//
//    @Inject
//    private Pain002XMLDAO pain002XMLDAO;
//
//    @Inject
//    private PaymentMessageDAO paymentMessageDAO;
//
//    @Inject
//    private PaymentInformationDAO paymentInformationDAO;
//
//    @Inject
//    private PaymentTransactionDAO paymentTransactionDAO;
//
//    @Inject
//    private StatusDAO statusDAO;
//
//    @Test
//    public void testRepository() {
//
//        pain001XMLDAO.save(Pain001XML.builder().id("1").xmlData("XML").createdDate(new Date()).build());
//        Pain001XML pain001XML = pain001XMLDAO.findOne("1");
//
//        Assert.notNull(pain001XML);
//        Assert.hasText("XML", pain001XML.getXmlData());
//
//        Status status = Status.builder()
//                .code("Ubehandlet")
//                .statusDefinition("Ubehandlet betaling element")
//                .build();
//        statusDAO.save(status);
//
//        Assert.notNull(statusDAO.findOne(status.getCode()));
//
//        pain002XMLDAO.save(Pain002XML.builder()
//                .xmlData("XML")
//                .pain001(pain001XML)
//                .sentDate(new Date())
//                .status(statusDAO.findOne(status.getCode()))
//                .build());
//
//        List<Pain002XML> pain002XML = pain002XMLDAO.findByPain001Id("1");
//
//        Assert.notNull(pain002XMLDAO.findByPain001Id("1"));
//        Assert.isTrue(pain002XMLDAO.findAll().size() == 1);
//        Assert.hasText("XML", pain002XML.get(0).getXmlData());
//
//        status = Status.builder()
//                .code("behandlet")
//                .statusDefinition("Behandlet betaling element")
//                .build();
//        statusDAO.save(status);
//
//        Assert.notNull(statusDAO.findOne(status.getCode()));
//
//        paymentMessageDAO.save(PaymentMessage.builder()
//                .id("4")
//                .xmlData("XML")
//                .numberOfTransactions(1)
//                .bic("BIC")
//                .createdDate(new Date())
//                .messagename("Test message")
//                .transactionSum(BigDecimal.valueOf(1000000L))
//                .status(statusDAO.findOne(status.getCode()))
//                .build());
//
//        Assert.notNull(paymentMessageDAO.findOne("4"));
//        Assert.isTrue(paymentMessageDAO.findAll().size() == 1);
//        Assert.hasText("XML", paymentMessageDAO.findOne("4").getXmlData());
//        Assert.hasText("BIC", paymentMessageDAO.findOne("4").getBic());
//        Assert.hasText("Test message", paymentMessageDAO.findOne("4").getMessagename());
//        Assert.isTrue(paymentMessageDAO.findOne("4").getTransactionSum().longValue() == 1000000L);
//
//        paymentInformationDAO.save(PaymentInformation.builder()
//                .id("3")
//                .debitAccountNumber("400000 00000")
//                .paymentMessage(paymentMessageDAO.findOne("4"))
//                .numberOfTransactions(1)
//                .paymentTransactionList(Arrays.asList(paymentTransactionDAO.findOne("2")))
//                .transactionsSum((BigDecimal.valueOf(1000000L)))
//                .status(statusDAO.findOne(status.getCode()))
//                .build());
//
//        Assert.notNull(paymentInformationDAO.findOne("3"));
//        Assert.hasText("400000 00000", paymentInformationDAO.findOne("3").getDebitAccountNumber());
//        Assert.isTrue(paymentInformationDAO.findOne("3").getNumberOfTransactions() == 1);
//        Assert.isTrue(paymentInformationDAO.findOne("3").getPaymentMessage().getId().equals("4"));
//        Assert.isTrue(paymentInformationDAO.findOne("3").getTransactionsSum().longValue() == 1000000L);
//
//        paymentTransactionDAO.save(PaymentTransaction.builder()
//                .id("2")
//                .accountNumber("9000000 00000")
//                .amount(BigDecimal.valueOf(1000L))
//                .name("Nav Bruker")
//                .paymentInformation(paymentInformationDAO.findOne("3"))
//                .status(status)
//                .build());
//
//        Assert.notNull(paymentTransactionDAO.findOne("2"));
//        Assert.hasText("Nav Bruker", paymentTransactionDAO.findOne("2").getName());
//        Assert.hasText("9000000 00000", paymentTransactionDAO.findOne("2").getAccountNumber());
//        Assert.isTrue(paymentTransactionDAO.findOne("2").getAmount().longValue() == 1000L);
//        Assert.isTrue(paymentTransactionDAO.findOne("2").getPaymentInformation().getId().equals("3"));
//
//    }
}