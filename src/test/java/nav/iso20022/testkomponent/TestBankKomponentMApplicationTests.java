package nav.iso20022.testkomponent;

import iso.std.iso._20022.tech.xsd.pain_001_001.Document;
import nav.iso20022.TestBankKomponentMApplication;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.InputStream;



@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration
@SpringBootTest(classes = TestBankKomponentMApplication.class)
public class TestBankKomponentMApplicationTests {

	@Value("classpath:/iso20022/pain001_20022DNB.xml")
	Resource xml;

	@Test
	public void contextLoads() {
		Assert.assertTrue(true);

		//InputSupplier<InputStream> input = xml.getInputStream();

				//Resources.newInputStreamSupplier(Resources.getResource("camt.053.001.02.xml"));

	}

}
