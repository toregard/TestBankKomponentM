package nav.iso20022.service;

import nav.iso20022.contrakt.ProcessService;
import nav.iso20022.domain.Pain001;
import nav.iso20022.repository.Pain001Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Transactional
public class ProcessImpl implements ProcessService {
    //@Autowired
    Pain001Repository pain001Repository;

    @Override
    public String processPain001(String paint001xml) {
        Pain001 record = new Pain001();
        record.setId(UUID.randomUUID().toString());
        record.setXml(paint001xml);
        pain001Repository.saveAndFlush(record);

        return "OK";
    }

}
