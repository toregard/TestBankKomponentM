package nav.iso20022.service;

import nav.iso20022.contrakt.JmsClient;
import nav.iso20022.jms.JmsConsumer;
import nav.iso20022.jms.JmsProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * http://javasampleapproach.com/spring-framework/spring-boot/use-spring-jms-activemq-jms-consumer-jms-producer-spring-boot
 */
@Service
public class JmsClientImpl implements JmsClient {

    //@Autowired
    JmsConsumer jmsConsumer;

    //@Autowired
    JmsProducer jmsProducer;

    @Override
    public void send(String msg) {
        jmsProducer.send(msg);
    }

    @Override
    public String receive() {
        return jmsConsumer.receive();
    }
}
