package nav.iso20022.contrakt;


public interface JmsClient {
    public void send(String msg);
    public String receive();
}
