package nav.iso20022.config;

import iso.std.iso._20022.tech.xsd.pain_001_001.Document;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.configuration.support.JobRegistryBeanPostProcessor;
import org.springframework.batch.core.converter.DefaultJobParametersConverter;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.xml.StaxEventItemReader;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class JobConfiguration implements ApplicationContextAware {

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	@Autowired
	public JobExplorer jobExplorer;

	@Autowired
	public JobRepository jobRepository;

	@Autowired
	public JobRegistry jobRegistry;

	@Autowired
	public JobLauncher jobLauncher;

	private ApplicationContext applicationContext;

	@Bean
	public JobRegistryBeanPostProcessor jobRegistrar() throws Exception {
		JobRegistryBeanPostProcessor registrar = new JobRegistryBeanPostProcessor();
		registrar.setJobRegistry(this.jobRegistry);
		registrar.setBeanFactory(this.applicationContext.getAutowireCapableBeanFactory());
		registrar.afterPropertiesSet();

		return registrar;
	}

	@Bean
	public JobOperator jobOperator() throws Exception {
		SimpleJobOperator simpleJobOperator = new SimpleJobOperator();
		simpleJobOperator.setJobLauncher(this.jobLauncher);
		simpleJobOperator.setJobParametersConverter(new DefaultJobParametersConverter());
		simpleJobOperator.setJobRepository(this.jobRepository);
		simpleJobOperator.setJobExplorer(this.jobExplorer);
		simpleJobOperator.setJobRegistry(this.jobRegistry);

		simpleJobOperator.afterPropertiesSet();

		return simpleJobOperator;
	}

	@Bean
	@StepScope
	public Tasklet tasklet(@Value("#{jobParameters['name']}") String name) {
//		return (stepContribution, chunkContext) -> {
//			//System.out.println(name);
//			return RepeatStatus.FINISHED;
//		};
		return new Tasklet() {
			@Override
			public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
				System.out.println(String.format("*****The job ran for %s", name));
				return RepeatStatus.FINISHED;
			}
		};
	}

	@Bean
	public Job job() {
		return jobBuilderFactory.get("job")
				.start(stepBuilderFactory.get("step1")
				.tasklet(tasklet(null))
					.build())
				.build();
	}

	@Bean
	ItemReader<Document> xmlFileItemReader() {
		StaxEventItemReader<Document> xmlFileReader = new StaxEventItemReader<>();
		xmlFileReader.setResource(new ClassPathResource("iso20022/pain001_20022DNB.xml"));
		xmlFileReader.setFragmentRootElementName("GrpHdr");

		Jaxb2Marshaller studentMarshaller = new Jaxb2Marshaller();
		studentMarshaller.setClassesToBeBound(Document.class);
		xmlFileReader.setUnmarshaller(studentMarshaller);
		return xmlFileReader;
	}
//
//	@Bean
//	public Job job() {
//		return jobBuilderFactory.get("job")
//				.start(step1())
//				.build();
//	}

//	@Bean
//	public Step step1() {
//		return stepBuilderFactory.get("step1")
//				.<GroupHeader32, GroupHeader32>chunk(10)
//				.reader(customerItemReader())
//				.writer(customerItemWriter())
//				.build();
//	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

//	@Bean
//	public StaxEventItemReader<GroupHeader32> customerItemReader() {
//        //CreditTransferTransactionInformation10
//		//XStreamMarshaller unmarshaller = new XStreamMarshaller();
//		Jaxb2Marshaller unmarshaller = new Jaxb2Marshaller();
//		unmarshaller.setContextPath("iso.std.iso._20022.tech.xsd.pain_001_001"); //;setClassesToBeBound( GroupHeader32.class, CreditTransferTransactionInformation10.class );
//		//unmarshaller.getClassesToBeBound()
////		Map<String, Class> aliases = new HashMap<>();
////		aliases.put("GroupHeader32", GroupHeader32.class);
////
////		unmarshaller.setAliases(aliases);
//
//		StaxEventItemReader<GroupHeader32> reader = new StaxEventItemReader<>();
//
//		reader.setResource(new ClassPathResource("/iso20022/pain001_20022DNB.xml"));
//		//reader.setFragmentRootElementName("PmtInf");
//		reader.setUnmarshaller(unmarshaller);
//
//		return reader;
//	}
//
//	@Bean
//	public ItemWriter<GroupHeader32> customerItemWriter() {
//		return items -> {
//			for (GroupHeader32 item : items) {
//				System.out.println(item.toString());
//			}
//		};
//	}
}
