package nav.iso20022.entities;

import java.math.BigDecimal;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface PaymentInformationDAO extends JpaRepository<PaymentInformation, String> {

    List<PaymentInformation> findByDebitAccountNumber(String debitAccountNumber);

    List<PaymentInformation> findByTransactionsSum(BigDecimal transactionsSum);

    List<PaymentInformation> findByTransactionsSumGreaterThan(BigDecimal transactionsSum);

    List<PaymentInformation> findByTransactionsSumGreaterThanEqual(BigDecimal transactionsSum);

    List<PaymentInformation> findByTransactionsSumLessThan(BigDecimal transactionsSum);

    List<PaymentInformation> findByTransactionsSumLessThanEqual(BigDecimal transactionsSum);

    @Query("Select p from PaymentInformation p where p.paymentMessage =?1")
    List<PaymentInformation> findByPaymentMessage(PaymentMessage paymentMessage);

    List<PaymentInformation> findByPaymentMessageId(String id);

    @Query("Select p from PaymentInformation p where p.status =?1")
    List<PaymentInformation> findByStatus(Status status);

    List<PaymentInformation> findByStatusCode(String statusCode);

}
