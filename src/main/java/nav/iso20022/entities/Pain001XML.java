package nav.iso20022.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PAIN001_XML")
public class Pain001XML implements Serializable {

    private static final long serialVersionUID = -3966047090748691847L;

    @Id
    @Column(name = "MESSAGE_ID", unique = true, nullable = false)
    private String id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "XML_DATA")
    private String xmlData;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MESSAGE_ID")
    private PaymentMessage paymentMessage;
}
