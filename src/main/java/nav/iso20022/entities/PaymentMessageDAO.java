package nav.iso20022.entities;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface PaymentMessageDAO extends JpaRepository<PaymentMessage, String> {

    List<PaymentMessage> findByBic(String bic);

    List<PaymentMessage> findByMessagename(String messagename);

    List<PaymentMessage> findByTransactionSum(BigDecimal transactionSum);

    List<PaymentMessage> findByTransactionSumGreaterThan(BigDecimal transactionSum);

    List<PaymentMessage> findByTransactionSumGreaterThanEqual(BigDecimal transactionSum);

    List<PaymentMessage> findByTransactionSumLessThan(BigDecimal transactionSum);

    List<PaymentMessage> findByTransactionSumLessThanEqual(BigDecimal transactionSum);

    List<PaymentMessage> findByCreatedDate(Date createdDate);

    List<PaymentMessage> findByCreatedDateGreaterThan(Date createdDate);

    List<PaymentMessage> findBycreatedDateGreaterThanEqual(Date createdDate);

    List<PaymentMessage> findByCreatedDateLessThan(Date createdDate);

    List<PaymentMessage> findByCreatedDateLessThanEqual(Date createdDate);

    @Query("Select p from PaymentMessage p where p.status =?1")
    List<PaymentMessage> findByStatus(Status status);

    List<PaymentMessage> findByStatusCode(String statusCode);

}
