package nav.iso20022.entities;

import java.math.BigDecimal;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface PaymentTransactionDAO extends JpaRepository<PaymentTransaction, String> {

    List<PaymentTransaction> findByAccountNumber(String accountNumber);

    List<PaymentTransaction> findByName(String name);

    List<PaymentTransaction> findByAmount(BigDecimal amount);

    List<PaymentTransaction> findByAmountGreaterThan(BigDecimal amount);

    List<PaymentTransaction> findByAmountGreaterThanEqual(BigDecimal amount);

    List<PaymentTransaction> findByAmountLessThan(BigDecimal amount);

    List<PaymentTransaction> findByAmountLessThanEqual(BigDecimal amount);

    @Query("Select p from PaymentTransaction p where p.paymentInformation =?1")
    List<PaymentTransaction> findByPaymentInformation(PaymentInformation paymentInformation);

    List<PaymentTransaction> findByPaymentInformationId(String paymentInformationId);

    @Query("Select p from PaymentTransaction p where p.status =?1")
    List<PaymentTransaction> findByStatus(Status status);

    List<PaymentTransaction> findByStatusCode(String statusCode);

}
