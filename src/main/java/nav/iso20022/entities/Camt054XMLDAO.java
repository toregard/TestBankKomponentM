package nav.iso20022.entities;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface Camt054XMLDAO extends JpaRepository<Camt054XML, Long> {

    List<Camt054XML> findByPaymentDate(Date paymentDate);

    List<Camt054XML> findByPaymentDateGreaterThan(Date paymentDate);

    List<Camt054XML> findByPaymentDateGreaterThanEqual(Date paymentDate);

    List<Camt054XML> findByPaymentDateLessThan(Date paymentDate);

    List<Camt054XML> findByPaymentDateLessThanEqual(Date paymentDate);

    @Query("Select c from Camt054XML c where c.status =?1")
    List<Camt054XML> findByStatus(Status status);

    List<Camt054XML> findByStatusCode(String statusCode);

}