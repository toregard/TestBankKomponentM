package nav.iso20022.entities;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@Repository
public interface Pain002XMLDAO extends JpaRepository<Pain002XML, Long> {

    List<Pain002XML> findByPain001Id(String messageId);

    List<Pain002XML> findBySentDate(Date sentDate);

    List<Pain002XML> findBySentDateGreaterThan(Date sentDate);

    List<Pain002XML> findBySentDateGreaterThanEqual(Date sentDate);

    List<Pain002XML> findBySentDateLessThan(Date sentDate);

    List<Pain002XML> findBySentDateLessThanEqual(Date sentDate);

    @Query("Select p from Pain002XML p where p.status =?1")
    List<Pain002XML> findByStatus(Status status);

    List<Pain002XML> findByStatusCode(String statusCode);

}