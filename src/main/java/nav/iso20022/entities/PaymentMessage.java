package nav.iso20022.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.stereotype.Component;

@Component
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PAYMENT_MESSAGE")
public class PaymentMessage implements Serializable {

    private static final long serialVersionUID = -6375028985612963223L;

    @Id
    @Column(name = "MESSAGE_ID", unique = true, nullable = false)
    private String id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "NUMBER_OF_TRANSACTIONS")
    private Integer numberOfTransactions;

    @Column(name = "TRANSACTION_SUM")
    private BigDecimal transactionSum;

    @Column(name = "BIC")
    private String bic;

    @Column(name = "MESSAGE_NAME")
    private String messagename;

    @OneToMany(mappedBy = "paymentMessage", fetch = FetchType.LAZY)
    @Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
    private List<PaymentInformation> paymentInformationList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CODE")
    private Status status;

    @Transient
    private String xmlData;

    @OneToOne(mappedBy = "paymentMessage", fetch = FetchType.LAZY)
    @JoinColumn(name = "MESSAGE_ID")
    private Pain001XML pain001XML;

    public void addPaymentInformation(PaymentInformation paymentInformation) {
        paymentInformation.setPaymentMessage(this);
        paymentInformationList.add(paymentInformation);
    }

}
