package nav.iso20022.controller;

import nav.iso20022.contrakt.ProcessService;
import nav.iso20022.domain.Response;
import nav.iso20022.contrakt.JmsClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


//@RestController
public class Pain001Controller {
    @Autowired
    ProcessService processService;

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private JobOperator jobOperator;

    @Autowired
    private Job job;

    @Autowired
    JmsClient jsmClient;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    //http://localhost:8080/paint001/create /{request} (@PathVariable("request")
    @RequestMapping(value = "/paint001/startJob/{jobbname}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> startJob(@PathVariable String jobbname) throws Exception {
        //@RequestBody String request){
        logger.info("Valgt:" + jobbname);
        processService.processPain001(jobbname);
        Response response = new Response();
        response.setAnswer("Svar" + jobbname);

        JobParameters jobParameters = new JobParametersBuilder()
                        .addString("name", jobbname)
                        .toJobParameters();
        this.jobLauncher.run(job, jobParameters);

        return new ResponseEntity<String>(jobbname, HttpStatus.OK);
    }

    @RequestMapping(value = "/paint001/send/{melding}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> send(@PathVariable String melding) throws Exception {
        logger.info("jsmClient sender:" + melding);
        jsmClient.send(melding);
        return new ResponseEntity<String>(melding, HttpStatus.OK);
    }

    @RequestMapping(value = "/paint001/motta", method = RequestMethod.GET, produces = "application/json")
    //@ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<String> send() throws Exception {
        logger.info("jsmClient mottar:");
        return new ResponseEntity<String>(jsmClient.receive(), HttpStatus.OK);
    }
}
