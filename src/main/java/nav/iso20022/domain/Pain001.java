package nav.iso20022.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

@Entity
@Table(name="Pain001")
public class Pain001 {
    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    String id;
    @NotEmpty
    String xml;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }
}
