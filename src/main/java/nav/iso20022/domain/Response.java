package nav.iso20022.domain;

/**
 * Created by toregard on 23.09.2016.
 */
public class Response {
    private String answer;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
