package nav.iso20022.domain;

/**
 * Created by toregard on 23.09.2016.
 {
 "id": "Id",
 "name": "Name"
 }
 */
public class Request {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
