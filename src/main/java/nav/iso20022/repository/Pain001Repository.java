package nav.iso20022.repository;

import nav.iso20022.domain.Pain001;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Pain001Repository extends JpaRepository<Pain001, Long> {

}
