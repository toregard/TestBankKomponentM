package nav.iso20022.jms;

import nav.iso20022.contrakt.Paint001JobService;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

//@Component
public class AutoConsumer {

    @Autowired
    Paint001JobService paint001JobService;

    @JmsListener(destination = "mailbox", containerFactory = "myFactory")
    public void receiveMessage(String message) throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {

        paint001JobService.start(message);

    }
}
